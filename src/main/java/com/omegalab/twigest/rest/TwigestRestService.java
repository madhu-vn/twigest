package com.omegalab.twigest.rest;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Collections;
import java.util.List;

import org.apache.commons.lang.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DuplicateKeyException;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.omegalab.twigest.dao.Dao;
import com.omegalab.twigest.domain.JsonResponse;
import com.omegalab.twigest.domain.Tweet;
import com.omegalab.twigest.domain.User;
import com.omegalab.twigest.domain.UserList;
import com.omegalab.twigest.executor.DigestEmailerExecutor;
import com.omegalab.twigest.executor.TweetsRetrieverExecutor;
import com.omegalab.twigest.executor.TwitterUserInfoUpdaterExecutor;
import com.omegalab.twigest.util.Constants;
import com.omegalab.twigest.util.HtmlBuilder;
import com.omegalab.twigest.util.Util;

@RestController
public class TwigestRestService {

	@Autowired
	private Dao dao;
	@Autowired
	private TwitterUserInfoUpdaterExecutor executor;
	@Autowired
	private TweetsRetrieverExecutor tweetsRetrieverExecutor;
	@Autowired
	private DigestEmailerExecutor digestEmailerExecutor;

	@RequestMapping(value = "/user", method = RequestMethod.POST, produces = "application/json")
	public JsonResponse createUser(@RequestBody User user) {
		JsonResponse response = new JsonResponse();
		List<String> errors = user.validate();
		
		if (errors.isEmpty()) {
			try {
				dao.createUser(user);
				response.setResult(Constants.SUCCESS);
			} catch (DuplicateKeyException dkEx) {
				response.setResult(Constants.ERROR);
				response.setErrors(Collections.singletonList("User : " + user.getId() + " already exists"));
			}			
		} else {
			response.setResult(Constants.ERROR);
			response.setErrors(errors);
		}
		
		return response;
	}

	@RequestMapping(value = "/userlist", method = RequestMethod.POST, produces = "application/json")
	public JsonResponse createUserList(@RequestBody UserList userList) {
		dao.createUserList(userList);
		executor.updateTwitterUserIds();
		
		return new JsonResponse(Constants.SUCCESS);
	}

	@RequestMapping(value = "/userlist", method = RequestMethod.PUT, produces = "application/json")
	public JsonResponse updateUserList(@RequestBody UserList userList) {
		dao.updateUserList(userList);
		executor.updateTwitterUserIds();
		
		return new JsonResponse(Constants.SUCCESS);
	}

	@RequestMapping(value = "/pulltweets", method = RequestMethod.GET, produces = "application/json")
	public JsonResponse pullTweets() {
		tweetsRetrieverExecutor.pullTweets();
		return new JsonResponse(Constants.SUCCESS);
	}

	@RequestMapping(value = "/senddigest", method = RequestMethod.GET, produces = "application/json")
	public JsonResponse sendDigest(
			@RequestParam(value = "date", required = false) String date) {
		if (StringUtils.isNotBlank(date)) {
			try {
				validateDate(date);
			} catch (ParseException parseErr) {
				throw new IllegalArgumentException(
						"Date has to be in the format : yyyy-MM-dd");
			}
		}

		digestEmailerExecutor.sendDigest(StringUtils.isBlank(date) ? Util
				.getYesterdayDateString() : date);
		
		return new JsonResponse(Constants.SUCCESS);
	}

	@RequestMapping(value = "/generatetweetshtml", method = RequestMethod.GET, produces = { "text/html" })
	public String generateTweetsHtml(@RequestParam(value = "date") String date,
			@RequestParam(value = "user") String user) {
		
		List<Tweet> tweets = dao.getTweets(user.toLowerCase(), date);
		return HtmlBuilder.buildTweetsHtml(tweets);
	}

	private void validateDate(String date) throws ParseException {
		SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd");
		format.parse(date);
	}

}
