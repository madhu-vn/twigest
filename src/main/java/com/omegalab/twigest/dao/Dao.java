package com.omegalab.twigest.dao;

import java.util.List;
import java.util.Map;

import com.omegalab.twigest.domain.Tweet;
import com.omegalab.twigest.domain.TwitterUser;
import com.omegalab.twigest.domain.User;
import com.omegalab.twigest.domain.UserList;

public interface Dao {
	
	public User createUser(User user);
	public void createUserList(UserList userList);
	public void updateUserList(UserList userList);
	public UserList getUserList(String userId);
	public List<String> getScreenNamesWithoutAnId();
	public List<Long> getTwitterUserIds();
	public void saveTweets(List<Tweet> tweets);
	public void updateTwitterUserList(long listId, long sinceId);
	public Long getTwitterLisId();
	public List<User> getUsers();
	List<Tweet> getTweets(String userId, String date);
	List<String> getAllScreenNames();
	void updateTwitterUserInfo(Map<String, TwitterUser> screenNameUserMap);
}
