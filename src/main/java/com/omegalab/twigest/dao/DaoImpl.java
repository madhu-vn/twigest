package com.omegalab.twigest.dao;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.Set;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DataAccessException;
import org.springframework.jdbc.core.BatchPreparedStatementSetter;
import org.springframework.jdbc.core.BeanPropertyRowMapper;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.ResultSetExtractor;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import com.omegalab.twigest.domain.Tweet;
import com.omegalab.twigest.domain.TwitterUser;
import com.omegalab.twigest.domain.User;
import com.omegalab.twigest.domain.UserList;
import com.omegalab.twigest.util.Util;

@Repository("dao")
public class DaoImpl implements Dao {
	
	@Autowired
	public DaoImpl(JdbcTemplate jdbcTemplate) {
		this.jdbcTemplate = jdbcTemplate;
	}
	
	private JdbcTemplate jdbcTemplate;

	@Override
	public User createUser(User user) {
		jdbcTemplate.update("INSERT INTO user VALUES(?, ?, ?, ?)", user.getId(), user.getFirstName(), user.getLastName(), user.getEmail());
		return user;
	}

	@Override
	public void createUserList(UserList userList) {
		for (String twitterScreenName : userList.getTwitterScreenNames()) {
			jdbcTemplate.update("INSERT INTO user_list (user_id, twitter_screen_name) VALUES(?, ?)", userList.getUserId(), twitterScreenName);
		}
	}

	@Override
	public void updateUserList(UserList userList) {
		jdbcTemplate.update("DELETE FROM user_list WHERE user_id = ?", userList.getUserId());
		
		for (String twitterScreenName : userList.getTwitterScreenNames()) {
			jdbcTemplate.update("INSERT INTO user_list (user_id, twitter_screen_name) VALUES(?, ?)", userList.getUserId(), twitterScreenName);
		}
	}

	@Override
	public List<String> getScreenNamesWithoutAnId() {
		return jdbcTemplate.queryForList("SELECT ul.twitter_screen_name FROM user_list ul LEFT OUTER JOIN twitter_user tu ON ul.twitter_screen_name = tu.screen_name WHERE tu.id IS NULL", String.class);
	}
	
	@Override
	public List<String> getAllScreenNames() {
		return jdbcTemplate.queryForList("SELECT twitter_screen_name FROM user_list", String.class);
	}

	@Override
	@Transactional
	public void updateTwitterUserInfo(final Map<String, TwitterUser> screenNameUserMap) {
		jdbcTemplate.update("DELETE FROM twitter_user");
		jdbcTemplate.batchUpdate("INSERT INTO twitter_user VALUES(?, ?, ?, ?)", getObjArray(screenNameUserMap));
	}
	
	private List<Object[]> getObjArray(final Map<String, TwitterUser> screenNameUserMap) {
		List<Object[]> screenNameIdList = new ArrayList<>();
		Set<String> keys = screenNameUserMap.keySet();
		
		for (String screenName : keys) {
			TwitterUser user = screenNameUserMap.get(screenName);
			screenNameIdList.add(new Object[] {screenName, user.getId(), user.getName(), user.getProfileImageUrl()});
		}
		
		return screenNameIdList;
	}

	@Override
	public List<Long> getTwitterUserIds() {
		return jdbcTemplate.queryForList("SELECT DISTINCT tu.id FROM user_list ul JOIN twitter_user tu ON ul.twitter_screen_name = tu.screen_name", Long.class);
//		return jdbcTemplate.queryForList("SELECT tu.id FROM user_list ul JOIN twitter_user tu ON ul.twitter_screen_name = tu.screen_name WHERE NOT EXISTS (SELECT 1 FROM twitter_user_list tul ON tu.id = tul.user_id)", Long.class);
	}
	
	@Override
	public Long getTwitterLisId() {
		return jdbcTemplate.queryForObject("SELECT list_id FROM twitter_user_list GROUP BY list_id HAVING count(*) < 5000", Long.class);
	}

	@Override
	public void saveTweets(final List<Tweet> tweets) {
		jdbcTemplate.batchUpdate("INSERT IGNORE INTO TWEET VALUES(?, ?, ?, ?, ?, ?)", new BatchPreparedStatementSetter() {
			
			@Override
			public void setValues(PreparedStatement ps, int index) throws SQLException {
				Tweet tweet = tweets.get(index);
				int columnIndex = 1;
				ps.setLong(columnIndex, tweet.getId());
				ps.setLong(++columnIndex, tweet.getUserId());
				ps.setTimestamp(++columnIndex, new java.sql.Timestamp(tweet.getCreated().getTime()));
				ps.setString(++columnIndex, tweet.getText());
				ps.setString(++columnIndex, tweet.getName());
				ps.setString(++columnIndex, tweet.getHandle());
			}
			
			@Override
			public int getBatchSize() {
				return tweets.size();
			}
		});	
	}

	@Override
	@Transactional
	public void updateTwitterUserList(long listId, long sinceId) {		
		jdbcTemplate.update("DELETE FROM twitter_user_list WHERE list_id = ?", listId);
		jdbcTemplate.update("INSERT INTO twitter_user_list VALUES(?, ?)", listId, sinceId);
	}

	@Override
	public UserList getUserList(final String userId) {
		return jdbcTemplate.query("SELECT twitter_screen_name AS screenname FROM user_list where user_id = ?", new Object[] {userId}, new ResultSetExtractor<UserList>() {

			@Override
			public UserList extractData(ResultSet resultset) throws SQLException, DataAccessException {
				UserList userList = new UserList();
				userList.setUserId(userId);
				List<String> screenNames = new ArrayList<>();
				while (resultset.next()) {
					screenNames.add(resultset.getString("screenname"));
				}
				userList.setTwitterScreenNames(Util.toArray(screenNames));
				
				return userList;
			}
		});
	}

	@Override
	public List<Tweet> getTweets(final String userId, final String date) {
		String sql = "SELECT t.id AS id, t.user_id AS userId, handle, tu.name AS name, tu.profile_image_url AS profileImgUrl, t.text AS text FROM tweet t "
					+ " JOIN twitter_user tu ON t.user_id = tu.id JOIN user_list ul ON ul.twitter_screen_name = tu.screen_name "
					+ " WHERE LOWER(ul.user_id) = ? AND DATE(`created`) = ? ORDER BY t.handle, t.created DESC";
		
		return jdbcTemplate.query(sql, new Object[] {userId, date}, new BeanPropertyRowMapper<Tweet>(Tweet.class));
	}

	@Override
	public List<User> getUsers() {
		return jdbcTemplate.query("SELECT id, first_name as firstName, last_name as lastName, email FROM user", new BeanPropertyRowMapper<User>(User.class));
	}

}
