package com.omegalab.twigest.domain;

import java.util.List;

public class JsonResponse {
	
	private String result;
	private List<String> errors;
	
	public JsonResponse() {
	}
	
	public JsonResponse(String result) {
		this.result = result;
	}
	
	public String getResult() {
		return result;
	}
	public void setResult(String result) {
		this.result = result;
	}
	public List<String> getErrors() {
		return errors;
	}
	public void setErrors(List<String> errors) {
		this.errors = errors;
	}

}
