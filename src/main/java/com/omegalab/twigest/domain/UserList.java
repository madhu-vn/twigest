package com.omegalab.twigest.domain;

public class UserList {

	private String userId;
	private String[] twitterScreenNames;
	
	public UserList() {
		
	}

	public UserList(String userId, String[] twitterScreenNames) {
		this.userId = userId;
		this.twitterScreenNames = twitterScreenNames;
	}

	public String getUserId() {
		return userId;
	}

	public void setUserId(String userId) {
		this.userId = userId;
	}

	public String[] getTwitterScreenNames() {
		return twitterScreenNames;
	}

	public void setTwitterScreenNames(String[] twitterScreenNames) {
		this.twitterScreenNames = twitterScreenNames;
	}

}
