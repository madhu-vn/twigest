package com.omegalab.twigest.domain;

public class TwitterUser {
	
	private String screenName;
	private long id;
	private String name;
	private String profileImageUrl;
	
	public TwitterUser(String screenName, long id, String name,
			String profileImageUrl) {
		super();
		this.screenName = screenName;
		this.id = id;
		this.name = name;
		this.profileImageUrl = profileImageUrl;
	}

	public String getScreenName() {
		return screenName;
	}

	public long getId() {
		return id;
	}

	public String getName() {
		return name;
	}

	public String getProfileImageUrl() {
		return profileImageUrl;
	}

}
