package com.omegalab.twigest.domain;

import java.util.Date;

public class Tweet {

	private long id;
	private long userId;
	private Date created;
	private String text;
	private String name;
	private String handle;
	private String profileImgUrl;
	
	public Tweet() {		
	}

	public Tweet(long id, long userId, Date created, String text, String handle, String name) {
		super();
		this.id = id;
		this.userId = userId;
		this.created = created;
		this.text = text;
		this.name = name;
		this.handle = handle;
	}
	
	public Tweet(long id, long userId, Date created, String text, String handle, String name, String profileImgUrl) {
		super();
		this.id = id;
		this.userId = userId;
		this.created = created;
		this.text = text;
		this.name = name;
		this.handle = handle;
		this.profileImgUrl = profileImgUrl;
	}
	
	public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}
	
	public long getUserId() {
		return userId;
	}

	public void setUserId(long userId) {
		this.userId = userId;
	}

	public Date getCreated() {
		return created;
	}

	public void setCreated(Date created) {
		this.created = created;
	}

	public String getText() {
		return text;
	}

	public void setText(String text) {
		this.text = text;
	}
	
	public String getHandle() {
		return handle;
	}

	public void setHandle(String handle) {
		this.handle = handle;
	}
	
	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getProfileImgUrl() {
		return profileImgUrl;
	}

	public void setProfileImgUrl(String profileImgUrl) {
		this.profileImgUrl = profileImgUrl;
	}
	
}
