package com.omegalab.twigest.domain;

import java.util.ArrayList;
import java.util.List;

import com.omegalab.twigest.util.EmailValidator;

public class User {
	
	private String id;
	private String firstName;
	private String lastName;
	private String email;
	
	public User() {
	}
	
	public User(String id, String firstName, String lastName, String email) {
		super();
		this.id = id;
		this.firstName = firstName;
		this.lastName = lastName;
		this.email = email;
	}

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getFirstName() {
		return firstName;
	}

	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}

	public String getLastName() {
		return lastName;
	}

	public void setLastName(String lastName) {
		this.lastName = lastName;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public List<String> validate() {
		List<String> errors = new ArrayList<>();
		if (!new EmailValidator().validate(email)) {
			errors.add("Email : " + email + " is invalid");
		}
		
		return errors;
	}
	
}
