package com.omegalab.twigest.executor;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

import javax.annotation.Resource;

import org.apache.log4j.Logger;
import org.springframework.beans.BeansException;
import org.springframework.context.ApplicationContext;
import org.springframework.context.ApplicationContextAware;
import org.springframework.mail.javamail.JavaMailSenderImpl;
import org.springframework.stereotype.Component;

import com.omegalab.twigest.dao.Dao;
import com.omegalab.twigest.domain.User;
import com.omegalab.twigest.job.DigestEmailer;

@Component
public class DigestEmailerExecutor implements ApplicationContextAware {

	private static final Logger LOGGER = Logger.getLogger(DigestEmailerExecutor.class);
	
	private ApplicationContext appCtx;
	private static ExecutorService THREAD_POOL = Executors.newFixedThreadPool(1);
	
	@Resource(name = "smtpGmailSender")
	private JavaMailSenderImpl mailSender;
	
	public void sendDigest(String date) {
		Dao dao = (Dao) appCtx.getBean("dao");
		List<List<User>> usersList = splitUsers(dao.getUsers());
		
		for (List<User> users : usersList) {
			THREAD_POOL.submit(new DigestEmailer(appCtx, mailSender, users, date));
		}
	}

	@Override
	public void setApplicationContext(ApplicationContext appCtx) throws BeansException {
		this.appCtx = appCtx;
	}
	
	private List<List<User>> splitUsers(List<User> users) {
		List<List<User>> usersList = new ArrayList<>();
		usersList.add(users);
		
		return usersList;
	}

}
