package com.omegalab.twigest.executor;

import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

import org.apache.log4j.Logger;
import org.springframework.beans.BeansException;
import org.springframework.context.ApplicationContext;
import org.springframework.context.ApplicationContextAware;
import org.springframework.stereotype.Component;

import com.omegalab.twigest.job.TwitterUserInfoUpdater;

@Component
public class TwitterUserInfoUpdaterExecutor implements ApplicationContextAware {
	
	private static final Logger LOGGER = Logger.getLogger(TwitterUserInfoUpdaterExecutor.class);
	
	private ApplicationContext appCtx;
	private static ExecutorService THREAD_POOL = Executors.newFixedThreadPool(1);
	
	public void updateTwitterUserIds() {
		THREAD_POOL.submit(new TwitterUserInfoUpdater(appCtx));
	}

	@Override
	public void setApplicationContext(ApplicationContext appCtx) throws BeansException {
		this.appCtx = appCtx;
	}

}
