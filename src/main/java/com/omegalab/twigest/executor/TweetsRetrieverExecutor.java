package com.omegalab.twigest.executor;

import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

import org.apache.log4j.Logger;
import org.springframework.beans.BeansException;
import org.springframework.context.ApplicationContext;
import org.springframework.context.ApplicationContextAware;
import org.springframework.stereotype.Component;

import com.omegalab.twigest.job.TweetsRetriever;

@Component
public class TweetsRetrieverExecutor implements ApplicationContextAware {

	private static final Logger LOGGER = Logger.getLogger(TweetsRetrieverExecutor.class);
	
	private ApplicationContext appCtx;
	private static ExecutorService THREAD_POOL = Executors.newFixedThreadPool(1);
	
	public void pullTweets() {
		THREAD_POOL.submit(new TweetsRetriever(appCtx));
	}

	@Override
	public void setApplicationContext(ApplicationContext appCtx) throws BeansException {
		this.appCtx = appCtx;
	}
	
}
