package com.omegalab.twigest.job;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.Callable;

import org.apache.log4j.Logger;
import org.springframework.context.ApplicationContext;

import com.omegalab.twigest.dao.Dao;
import com.omegalab.twigest.domain.Tweet;
import com.omegalab.twigest.util.Constants;
import com.omegalab.twigest.util.TwitterUtil;

import twitter4j.Paging;
import twitter4j.ResponseList;
import twitter4j.Status;
import twitter4j.Twitter;
import twitter4j.UserList;

public class TweetsRetriever implements Callable<String> {

	private static final Logger LOGGER = Logger.getLogger(TweetsRetriever.class);

	private ApplicationContext context;
	private Twitter twitter = TwitterUtil.getTwitter();

	public TweetsRetriever(ApplicationContext context) {
		this.context = context;
	}

	@Override
	public String call() throws Exception {
		try {
			retrieveTweets();
		} catch (Exception e) {
			LOGGER.error("Exception while retrieving the tweets", e);
			throw e;
		}

		return "success";
	}

	private void retrieveTweets() throws Exception {
		Dao dao = (Dao) context.getBean("dao");
		destroyUserLists();
		Thread.sleep(5000);
		List<UserList> userLists = createUserLists(dao);

		LOGGER.info("Total UserLists : " + userLists.size());
		
		Thread.sleep(5000);

		for (UserList userList : userLists) {
			int page = 0;
			long sinceId = 0;
			List<Tweet> tweets = new ArrayList<>();

			while (true) {
				ResponseList<Status> list = twitter.getUserListStatuses(userList.getId(),
						new Paging(++page).count(Constants.TWEETS_TO_RETRIEVE));

				LOGGER.info("Number of tweets :" + list.size());
				
				if (list.size() == 0) {
					break;
				}

				for (Status tweet : list) {
					if (tweets.size() == Constants.MAX_TWEETS_TO_STORE) {
						dao.saveTweets(tweets);
						tweets = new ArrayList<>();
					}

//					System.out.println(String.format(Constants.TWITTER_STATUS_URL, tweet.getUser().getScreenName(), tweet.getId()));
//					OEmbedRequest req = new OEmbedRequest(tweet.getId(), String.format(Constants.TWITTER_STATUS_URL, tweet.getUser().getScreenName(), tweet.getId()));
//					req.HideMedia(false);
//					req.omitScript(true);
//
//					OEmbed embedReq = twitter.tweets().getOEmbed(req);

					tweets.add(new Tweet(tweet.getId(), tweet.getUser().getId(), tweet.getCreatedAt(),
							tweet.getText(), tweet.getUser().getScreenName(), tweet.getUser().getName()));

					if (tweet.getId() > sinceId) {
						sinceId = tweet.getId();
					}
				}
			}

			dao.saveTweets(tweets);
		}
		
		LOGGER.info("Completed retrieving the tweets");
	}

	public void destroyUserLists() throws Exception {
		ResponseList<UserList> rl = twitter.getUserLists(twitter.showUser("twigestnow").getId());
		for (UserList userList : rl) {
			twitter.destroyUserList(userList.getId());
		}
	}

	public List<UserList> createUserLists(Dao dao) throws Exception {

		List<UserList> userLists = new ArrayList<>();
		List<List<Long>> twitterUsers = splitUsers(dao);

		int index = 0;
		for (List<Long> users : twitterUsers) {
			UserList userList = twitter.createUserList("tmptwigest" + index++, false, "temp list");
			Thread.sleep(5000);
			LOGGER.info("Adding " + users.size() + " to the UserList : " + userList.getName());
			twitter.createUserListMembers(userList.getId(), toArray(users));
			userLists.add(userList);
		}

		return userLists;
	}

	private List<List<Long>> splitUsers(Dao dao) {
		List<List<Long>> userLists = new ArrayList<>();

		List<Long> users = dao.getTwitterUserIds();
		int userListCount = (users.size() / Constants.MAX_USERS_IN_LIST)
				+ (users.size() % Constants.MAX_USERS_IN_LIST > 0 ? 1 : 0);
		for (int index = 0; index < userListCount;) {
			userLists.add(users.subList((Constants.MAX_USERS_IN_LIST * index),
					(Constants.MAX_USERS_IN_LIST * ++index) - 1 < users.size()
							? (Constants.MAX_USERS_IN_LIST * index) - 1 : users.size()));
		}

		return userLists;
	}

	private long[] toArray(List<Long> values) {
		long[] array = new long[values.size()];
		int index = 0;
		for (Long value : values) {
			array[index++] = value;
		}

		return array;
	}

}
