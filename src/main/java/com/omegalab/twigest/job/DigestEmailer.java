package com.omegalab.twigest.job;

import java.util.List;
import java.util.concurrent.Callable;

import javax.mail.internet.MimeMessage;

import org.apache.log4j.Logger;
import org.springframework.context.ApplicationContext;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.mail.javamail.MimeMessageHelper;

import com.omegalab.twigest.dao.Dao;
import com.omegalab.twigest.domain.Tweet;
import com.omegalab.twigest.domain.User;
import com.omegalab.twigest.util.HtmlBuilder;

public class DigestEmailer implements Callable<String> {

	private static final Logger LOGGER = Logger.getLogger(DigestEmailer.class);

	private String date;
	private List<User> users;
	private JavaMailSender mailSender;
	private ApplicationContext context;

	public DigestEmailer(ApplicationContext context, JavaMailSender mailSender, List<User> users, String date) {
		this.date = date;
		this.users = users;
		this.context = context;
		this.mailSender = mailSender;
	}

	@Override
	public String call() throws Exception {
		try {
			Dao dao = (Dao) context.getBean("dao");

			for (User user : users) {
				List<Tweet> tweets = dao.getTweets(user.getId(), date);
				String digest = HtmlBuilder.buildDigestHtml(user.getId(), date, tweets);
				
				sendEMail(digest, user);
				
				LOGGER.info("Sent the digest email to : " + user.getEmail());
			}
		} catch (Exception exception) {
			LOGGER.error("Exception while building the digest html template", exception);
			throw exception;
		}

		return "success";
	}

	private void sendEMail(final String digest, final User user) throws Exception {
		MimeMessage mimeMessage = mailSender.createMimeMessage();
		MimeMessageHelper message = new MimeMessageHelper(mimeMessage);
		message.setSubject("Twigest for " + date);
		message.setTo(user.getEmail());
		message.setText(digest, true);
		
		this.mailSender.send(mimeMessage);		
	}

}
