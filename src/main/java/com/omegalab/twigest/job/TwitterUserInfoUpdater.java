package com.omegalab.twigest.job;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.concurrent.Callable;

import org.apache.log4j.Logger;
import org.springframework.context.ApplicationContext;

import com.omegalab.twigest.dao.Dao;
import com.omegalab.twigest.domain.TwitterUser;
import com.omegalab.twigest.util.TwitterUtil;
import com.omegalab.twigest.util.Util;

import twitter4j.ResponseList;
import twitter4j.Twitter;
import twitter4j.User;

public class TwitterUserInfoUpdater implements Callable<String> {

	private static final Logger LOGGER = Logger.getLogger(TwitterUserInfoUpdater.class);

	private ApplicationContext context;

	public TwitterUserInfoUpdater(ApplicationContext context) {
		this.context = context;
	}

	@Override
	public String call() throws Exception {
		try {
			LOGGER.info("Starting the job to update the Twitter Users information");

			Dao dao = (Dao) context.getBean("dao");
			Map<String, TwitterUser> screenNameIdMap = new HashMap<>();
			Twitter twitter = TwitterUtil.getTwitter();
			List<String> screenNames = dao.getAllScreenNames();
			LOGGER.info("Twitter Users : " + screenNames.size());

			ResponseList<User> users = twitter.lookupUsers(Util.toArray(screenNames));

			if (users != null) {
				for (User user : users) {
					screenNameIdMap.put(user.getScreenName(), new TwitterUser(user.getScreenName(), user.getId(), user.getName(), user.getBiggerProfileImageURL()));
				}

				dao.updateTwitterUserInfo(screenNameIdMap);
			}

			LOGGER.info("Completed updating the Twitter Users Information");
		} catch (Exception exception) {
			LOGGER.error("Exception while updating the twitter users information", exception);
			throw exception;
		}

		return "success";
	}

}
