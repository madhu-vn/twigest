package com.omegalab.twigest.util;

import java.io.StringWriter;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.commons.lang.StringEscapeUtils;
import org.apache.log4j.Logger;
import org.apache.velocity.Template;
import org.apache.velocity.VelocityContext;
import org.apache.velocity.app.VelocityEngine;
import org.apache.velocity.runtime.RuntimeConstants;
import org.apache.velocity.runtime.resource.loader.ClasspathResourceLoader;

import com.omegalab.twigest.domain.Tweet;

public class HtmlBuilder {
	
	private static final Logger LOGGER = Logger.getLogger(HtmlBuilder.class);

	public static String buildDigestHtml(String user, String date, List<Tweet> tweets) {
		LOGGER.info("Total number of tweets " + tweets.size());
			
		VelocityEngine engine = new VelocityEngine();
		engine.setProperty(RuntimeConstants.RESOURCE_LOADER, "classpath");
		engine.setProperty("classpath.resource.loader.class", ClasspathResourceLoader.class.getName());
		engine.init();
		
		VelocityContext context = new VelocityContext();
		List<Map<String, String>> tweetsList = new ArrayList<>();
		Template template = engine.getTemplate("digest-template.vm");
		
		for (Tweet tweet : tweets) {
			Map<String, String> map = new HashMap<>();
			map.put("name", tweet.getName());
			map.put("id", String.valueOf(tweet.getId()));
			map.put("handle", tweet.getHandle());
			map.put("profileImgUrl", tweet.getProfileImgUrl());
			map.put("text", StringEscapeUtils.escapeHtml(tweet.getText()));

			tweetsList.add(map);
		}
		
		context.put("tweets", tweetsList);
		context.put("user", user);
		context.put("date", date);
		StringWriter writer = new StringWriter();
		template.merge(context, writer);

		return writer.toString();
	}
	
	public static String buildTweetsHtml(List<Tweet> tweets) {
		LOGGER.info("Total number of tweets " + tweets.size());
			
		VelocityEngine engine = new VelocityEngine();
		engine.setProperty(RuntimeConstants.RESOURCE_LOADER, "classpath");
		engine.setProperty("classpath.resource.loader.class", ClasspathResourceLoader.class.getName());
		engine.init();
		
		VelocityContext context = new VelocityContext();
		List<Map<String, String>> tweetsList = new ArrayList<>();
		Template template = engine.getTemplate("tweets-template.vm");
		
		for (Tweet tweet : tweets) {
			Map<String, String> map = new HashMap<>();
			map.put("name", tweet.getName());
			map.put("id", String.valueOf(tweet.getId()));
			map.put("handle", tweet.getHandle());
			map.put("profileImgUrl", tweet.getProfileImgUrl());
			map.put("text", StringEscapeUtils.escapeHtml(tweet.getText()));

			tweetsList.add(map);
		}
		
		context.put("tweets", tweetsList);
		StringWriter writer = new StringWriter();
		template.merge(context, writer);

		return writer.toString();
	}

}
