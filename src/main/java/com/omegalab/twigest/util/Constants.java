package com.omegalab.twigest.util;

public class Constants {

	public static final int MAX_USERS_IN_LIST = 100;
	public static final int MAX_TWEETS_TO_STORE = 5000;
	public static final int TWEETS_TO_RETRIEVE = 100;
	public static final String TWITTER_STATUS_URL = "https://twitter.com/%s/status/%s";
	
	public static final String SUCCESS = "success";
	public static final String ERROR = "error";
}
