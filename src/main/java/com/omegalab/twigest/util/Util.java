package com.omegalab.twigest.util;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.List;

public class Util {

	public static String[] toArray(List<String> values) {
		String[] array = new String[values.size()];
		int index = 0;
		for (String value : values) {
			array[index++] = value;
		}

		return array;
	}

	public static String getYesterdayDateString() {
		DateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd");
		Calendar cal = Calendar.getInstance();
		cal.add(Calendar.DATE, -1);
		return dateFormat.format(cal.getTime());
	}

}
